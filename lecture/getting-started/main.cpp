#include <iostream>

#include <string>

#include <sstream>

#include <ostream>

int inputAge(std::istream& in) {
	int age { -1 };
	if (in >> age) {
		return age;
	}
	std::cout << in.fail() << std::endl;
	return -1;
}

int inputAge2(std::istream & in) {
	while (in) {
		std::string line { };
		getline(in, line);
		std::istringstream is { line };
		int age { -1 };
		if (is >> age) {
			return age;
		}
		std::cout << in.fail() << std::endl;
	}
	std::cout << in.fail() << std::endl;
	return -1;
}

int main() {
	std::cout << inputAge2(std::cin) << std::endl;
	std::cout << std::cin << std::endl;
}
