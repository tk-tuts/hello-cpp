/*
 * Date.h
 *
 *  Created on: Dec 30, 2017
 *      Author: Keerthikan
 */

#ifndef DATE_H_
#define DATE_H_

#include <tuple>
#include <ostream>

class Date {
	int year, month, day;
public:
	Date(int year, int month, int day);
	Date() = default;
	int getYear() const;

	bool operator<(Date const &rhs){
		return std::tie(year, month, day) < std::tie(rhs.year, rhs.month, rhs.day);
	}

	std::ostream& print(std::ostream& os) const {
		os << year << "\n";
		return os;
	}

	virtual ~Date();
};

inline std::ostream& operator<<(std::ostream& os, Date const& date) {
	return date.print(os);
}

#endif /* DATE_H_ */
