#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include <iostream>

#include <string>

//Create a Structure to store MP3s tagged info
struct MP3Tag {
	std::string Artist;
	std::string Title;
	std::string Album;
	std::string Year;
	std::string Comment;
};

void WriteTitle(FILE *file, int filesize, char Title[]) {
	int writeloc = filesize - 125;
	fseek(file, writeloc, SEEK_SET);
	fwrite(Title, 1, 30, file); //goes to the storage of Title data and writes new data
	std::cout << Title << " set as The Title" << std::endl;
}

int getsize(FILE *file) {
	//returns the size of the file as an int
	int loc = ftell(file);
	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fseek(file, loc, SEEK_SET);
	return size;
}

int main() {
	WIN32_FIND_DATA FileData;
	HANDLE hSearch;
	DWORD dwAttrs;
	TCHAR szNewPath[MAX_PATH] = "";

	BOOL fFinished = FALSE;

	// Start searching for text files in the current directory.

	hSearch = FindFirstFile(TEXT("*.mp3"), &FileData);
	if (hSearch == INVALID_HANDLE_VALUE) {
		printf("No mp3 files found.\n");
		return 0;
	}

	while (!fFinished) {
		FILE *mFile;
		mFile = fopen(FileData.cFileName, "r");
		std::cout << FileData.cFileName;

		if (mFile == NULL)
			perror("Error opening file");
		else {
			int filesize = getsize(mFile); //stores the full file size needed for writing data later
			char title[] = "Local Boys from C++";
			WriteTitle(mFile, filesize, title); //same info as case 1
			std::cout << "File is opened";
			fclose(mFile);
		}

		if (!FindNextFile(hSearch, &FileData)) {
			if (GetLastError() == ERROR_NO_MORE_FILES) {
				fFinished = TRUE;
			} else {
				printf("Could not find next file.\n");
				return 0;
			}
		}
	}

	// Close the search handle. 

	FindClose(hSearch);
}

