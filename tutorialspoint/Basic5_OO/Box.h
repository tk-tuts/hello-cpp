//
// Created by Keerthikan on 11-Sep-17.
//

#ifndef BASIC5_OO_BOX_H
#define BASIC5_OO_BOX_H


class Box {
public:
    double breadth;
    double height;
    Box(double b); // Constructor
    Box (double b, double h);
    ~Box(); // destructor

    void setLength(double len);

    double getLength(void);

    double getVolume(void) {
        return this->length * breadth * height;
    }

private:
    double length;
};

void handleBoxClass(std::ostream &out);

#endif //BASIC5_OO_BOX_H
