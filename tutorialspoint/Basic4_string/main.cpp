#include <iostream>
#include <vector>
#include <string>

void workingWithStdString(std::ostream &out){
    out << "Working with String" << std::endl;

    std::string input{};
    std::cin >> input;

    out << "Input:" << input << std::endl;
}

int main() {
    std::cout << "String and Sequences" << std::endl;

    std::string name = "Jon Snow";
    std::vector<std::string> names{};
    names.push_back(name);
    names.push_back("Bran Stark");

    std::cout << "front:" << names.front() << std::endl;
    std::cout << "back:" << names.back() << std::endl;

    workingWithStdString(std::cout);

    return 0;
}

