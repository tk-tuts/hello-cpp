#include <iostream>
#include "header.h"
using namespace std;

int x;

int main() {
	x = 5;

	print_global_x();

	int i = -2;
	unsigned int y = i;
	cout << i << endl;
	cout << y << endl;

	int h = 2;
	unsigned int z = h;
	cout << h << endl;
	cout << z << endl;

	std::cin.ignore();
	return 0;
}